package com.hawkins.fireball.models;

import com.hawkins.fireball.builders.CompanyBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jhawkins on 7/21/16.
 */
public class CompanyTest {

    @Test
    public void testCompany() {
        Company company = new CompanyBuilder()
                .setId("id")
                .setName("name")
                .build();

        assertEquals("id", company.getId());
        assertEquals("name", company.getName());

        assertNotNull(company.toString());
    }
}
