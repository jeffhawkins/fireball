package com.hawkins.fireball.services;

import com.hawkins.fireball.datasources.UserDataSource;
import com.hawkins.fireball.models.User;
import com.hawkins.fireball.builders.UserBuilder;
import com.hawkins.fireball.validations.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by jhawkins on 7/21/16.
 */
public class UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private UserDataSource userDataSource;


    public UserService() {
        LOG.info("UserService::ctor");
        userDataSource = new UserDataSource();
    }


    public List<User> get(String company) {
        LOG.info("UserService::get");

        return userDataSource.query();
    }


    public void add(String companyId, String first, String last) {
        LOG.info("UserService::add");

        User user = new UserBuilder()
                .setCompanyId(companyId.trim())
                //.setFirstName(first.trim())
                //.setLastName(last.trim())
                .build();

        if (!UserValidator.isValid(user)) {
            throw new IllegalStateException("Invalid User");
        }

        userDataSource.put(user);
    }

    public void update(String id, String companyId, String first, String last) {
        LOG.info("UserService::update");

        User user = new UserBuilder()
                .setId(id)
                .setCompanyId(companyId.trim())
                //.setFirstName(first.trim())
                //.setLastName(last.trim())
                .build();

        if (!UserValidator.isValid(user)) {
            throw new IllegalStateException("Invalid User");
        }

        userDataSource.put(user);
    }


    @Override
    public String toString() {
        return "UserService";
    }
}
