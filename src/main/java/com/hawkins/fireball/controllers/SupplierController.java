package com.hawkins.fireball.controllers;

import com.hawkins.fireball.models.Supplier;
import com.hawkins.fireball.services.SupplierService;
import com.hawkins.fireball.utils.Constants;
import com.hawkins.fireball.utils.JsonTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;

/**
 * Created by jhawkins on 7/21/16.
 */
public class SupplierController extends BaseController {
    private static final Logger LOG = LoggerFactory.getLogger(SupplierController.class);
    private SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        LOG.info("SupplierController::ctor");
        this.supplierService = supplierService;


        get("/rest/v1/suppliers", (req, res) -> {
            String company = req.headers(Constants.ORGOID);

            return supplierService.query();
        }, new JsonTransformer());


        post("/rest/v1/suppliers", (req, res) -> {
            String body = req.body();
            if (StringUtils.isBlank(body) == true) {
                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing body.");
            }

            Map<String, String> map = getGson().fromJson(body, Map.class);

            supplierService.put(
                    map.get(Supplier.COMPANY_ID),
                    map.get(Supplier.CONTACT),
                    map.get(Supplier.NAME),
                    map.get(Supplier.ADDRESS),
                    map.get(Supplier.CITY),
                    map.get(Supplier.STATE),
                    map.get(Supplier.ZIP),
                    map.get(Supplier.PHONE),
                    map.get(Supplier.WEBSITE));

            return StringUtils.EMPTY;
        });

    }
}
