package com.hawkins.fireball.datasources;

import com.hawkins.fireball.models.Contractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;

import java.util.List;

/**
 * Created by jhawkins on 7/23/16.
 */
public class ContractorDataSource extends BaseDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(ContractorDataSource.class);

    public void put(Contractor contractor) {
        LOG.info("ContractorDataSource::put");

        String sql = "insert into contractors values(:id, :companyId, :category, :contact, :name, :address," +
                " :city, :state, :zip, :phone, :website)";

        try (Connection connection = getSql2o().open()) {
            connection.createQuery(sql)
                    .addParameter(Contractor.ID, getUUID())
                    .addParameter(Contractor.COMPANY_ID, contractor.getCompanyId())
                    .addParameter(Contractor.CATEGORY, contractor.getCategory())
                    .addParameter(Contractor.CONTACT, contractor.getContact())
                    .addParameter(Contractor.NAME, contractor.getName())
                    .addParameter(Contractor.ADDRESS, contractor.getAddress())
                    .addParameter(Contractor.CITY, contractor.getCity())
                    .addParameter(Contractor.STATE, contractor.getState())
                    .addParameter(Contractor.ZIP, contractor.getZip())
                    .addParameter(Contractor.PHONE, contractor.getPhone())
                    .addParameter(Contractor.WEBSITE, contractor.getWebsite())
                    .executeUpdate();
        }
    }


    public List<Contractor> query() {
        LOG.info("ContractorDataSource::query");

        String sql = "select id, companyId, category, contact, name, address, city, state, zip, phone, website from contractors";

        try (Connection connection = getSql2o().open()) {
            return connection.createQuery(sql).executeAndFetch(Contractor.class);
        }
    }
}
