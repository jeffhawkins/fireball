package com.hawkins.fireball.models;

/**
 * Created by jhawkins on 7/21/16.
 */
public class User {
    public static final String ID = "id";
    public static final String COMPANY_ID = "companyId";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String RESET_CODE = "resetCode";
    public static final String ADMIN = "admin";
    public static final String LOCKED = "locked";

    private String id;
    private String companyId;
    private String name;
    private String email;
    private String password;
    private String resetCode;
    private boolean admin;
    private boolean locked;

    public User() {}

    public User(String id, String companyId, String name, String email, String password) {
        this.id = id;
        this.companyId = companyId;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean isLocked() {
        return locked;
    }


    @Override
    public String toString() {
        return "Id=" + id +
                " CompanyId=" + companyId +
                " Name=" + name +
                " Email=" + email +
                " Password=" + password +
                " ResetCode=" + resetCode +
                " Admin=" + admin +
                " Locked=" + locked;
    }
}
