package com.hawkins.fireball.validations;

import com.hawkins.fireball.models.Contractor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jhawkins on 8/23/16.
 */
public class ContractorValidator {
    private static final Logger LOG = LoggerFactory.getLogger(ContractorValidator.class);
    private static final int FIELD_LENGTH = 100;


    public static boolean isValid(Contractor contractor) {
        LOG.info("ContractorValidator::isValid");

        if (contractor == null) {
            LOG.info("contractor is null");
            return false;
        }

        if (StringUtils.isEmpty(contractor.getCompanyId())) {
            LOG.info("contractor companyId is empty");
            return false;
        }

        if (StringUtils.isEmpty(contractor.getName())) {
            LOG.info("contractor name is empty");
            return false;
        }

        if (contractor.getName().length() > FIELD_LENGTH) {
            LOG.info("contractor name is too long");
            return false;
        }

        if (StringUtils.isEmpty(contractor.getContact())) {
            LOG.info("contractor contact is empty");
            return false;
        }

        if (contractor.getContact().length() > FIELD_LENGTH) {
            LOG.info("contractor contact is too long");
            return false;
        }

        if (StringUtils.isEmpty(contractor.getPhone())) {
            LOG.info("contractor phone is empty");
            return false;
        }

        return true;
    }
}
