package com.hawkins.fireball.validations;

import com.hawkins.fireball.models.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jhawkins on 8/23/16.
 */
public class UserValidator {
    private static final Logger LOG = LoggerFactory.getLogger(UserValidator.class);
    private static final int FIELD_LENGTH = 100;

    public static boolean isValid(User user) {
        LOG.info("UserValidator::isValid");

        if (user == null) {
            LOG.info("user is null");
            return false;
        }

        if (StringUtils.isEmpty(user.getName())) {
            LOG.info("user name is empty");
            return false;
        }

        if (user.getName().length() > FIELD_LENGTH) {
            LOG.info("user first is too long");
            return false;
        }

        if (StringUtils.isEmpty(user.getEmail())) {
            LOG.info("user email is empty");
            return false;
        }

        if (user.getEmail().length() > FIELD_LENGTH) {
            LOG.info("user last is too long");
            return false;
        }

        return true;
    }
}
