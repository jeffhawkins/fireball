'use strict';

angular.module('build').controller('IndexController', [
  '$scope',
  '$log',
  '$http',
  'AuthenticationService',
  function ($scope, $log, $http, AuthenticationService) {
    
    $scope.settings = {
      loggedIn: false
    };
    
    $scope.$watch(function() {
      return AuthenticationService.isSignedIn();
    }, function(newValue) {
      $scope.settings.loggedIn = newValue;
    });

  }
]);
