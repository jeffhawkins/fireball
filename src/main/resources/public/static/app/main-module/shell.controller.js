'use strict';

angular.module('build').controller('ShellController', [
  '$scope',
  '$log',
  '$http',
  'SignInService',
  'AlertService',
  'AuthenticationService',
  function ($scope, $log, $http, SignInService, AlertService, AuthenticationService) {
    $scope.settings = {
      signedIn: false
    };    

    $scope.onProfileClick = function() {
      $log.log('You clicked on the profile.');
      SignInService.signIn('jeff', 'yikes').then(function(response){
        $log.log('You won at life: ' + JSON.stringify(response));
      }, function(response){
        AlertService.showDangerMessage('Unable to show profile. ' + response.statusText);
      });
    };

    $scope.onSignInClick = function() {
      $log.log('You clicked on login.');
      AuthenticationService.signIn();
    };
    
    $scope.onSignOutClick = function() {
      $log.log('You clicked on logout.');
      AuthenticationService.signOut();
    };
    
    $scope.$watch(function() {
      return AuthenticationService.isSignedIn();
    }, function(newValue) {
      $scope.settings.signedIn = newValue;
    });
    
  }
]);
