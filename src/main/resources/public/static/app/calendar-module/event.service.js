'use strict';

angular.module('xfit.calendar').service('EventService', [
  '$log',
  '$q',
  '$http',
  function ($log, $q, $http) {

    this.all = function () {
      var d = $q.defer();

      $http.get('/blog/v1/events').success(function(data) {
        d.resolve(data);
      }).error(function(reason) {
        d.reject(reason);
      });

      return d.promise;
    };

    this.post = function (data) {
      var d = $q.defer();

      $http.post('/blog/v1/events', data).success(function(data) {
        d.resolve(data);
      }).error(function(reason) {
        d.reject(reason);
      });

      return d.promise;
    };

    this.put = function (data) {
      var d = $q.defer();

      $http.put('/blog/v1/events/' + data._id, data).success(function(data) {
        d.resolve(data);
      }).error(function(reason) {
        d.reject(reason);
      });

      return d.promise;
    };

    this.delete = function (data) {
      var d = $q.defer();

      $http.delete('/blog/v1/events/' + data._id).success(function(data) {
        d.resolve(data);
      }).error(function(reason) {
        d.reject(reason);
      });

      return d.promise;
    };
  }
]);