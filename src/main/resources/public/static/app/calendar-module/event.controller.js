'use strict';

angular.module('xfit.calendar').controller('EventController', [
  '$log',
  '$rootScope',
  '$scope',
  function ($log, $rootScope, $scope) {
    $scope.event = {
      description: '',
      start: new Date(),
      end: new Date()
    };
  }
]);
